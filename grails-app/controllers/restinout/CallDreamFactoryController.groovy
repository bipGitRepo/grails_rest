package restinout

import grails.converters.JSON
import groovyx.net.http.HTTPBuilder
import groovyx.net.http.Method
import groovyx.net.http.ContentType
import org.apache.http.auth.AuthScope
import org.apache.http.auth.UsernamePasswordCredentials


class CallDreamFactoryController {

    def session_id
    String imeiNum
    String simNum
    def resultReader

    def index() { }

    def sendmessage() {
        def httpSession = new HTTPBuilder('http://db.likepay.me/rest/user/session?app_name=checkReg')

        httpSession.client.getCredentialsProvider().setCredentials(
                new AuthScope("proxy.pbank.com.ua", 8080),
                new UsernamePasswordCredentials("dn200186dvn", "Bip7415963")
        )

        httpSession.setProxy('proxy.pbank.com.ua', 8080, 'http')

        httpSession.request(Method.POST, ContentType.JSON){ req ->
            body = ['email': 'thefestival24@gmail.com', 'password': 'likepaylikepay']

            response.success = { resp, json ->
                session_id = json.session_id
            }
        }

        imeiNum = request.JSON.IMEI
        simNum = request.JSON.SIM_SN
        println "imei: " + imeiNum
        println "simNum: " + simNum


        def httpGetPhone = new HTTPBuilder()

        httpGetPhone.client.getCredentialsProvider().setCredentials(
                new AuthScope("proxy.pbank.com.ua", 8080),
                new UsernamePasswordCredentials("dn200186dvn", "Bip7415963")
        )

        httpGetPhone.setProxy('proxy.pbank.com.ua', 8080, 'http')
        String getPhoneUri = 'http://db.likepay.me:80/rest/checkReg/registration?filter=IMEI%20%3D%20' + imeiNum + '%20and%20SIM_SN%20%3D%20' + simNum + '&fields=telNum'


        httpGetPhone.request(getPhoneUri, Method.GET, ContentType.JSON){req ->
            headers.'X-DreamFactory-Session-Token' = session_id
            headers.'X-DreamFactory-Application-Name' = 'checkReg'

            response.success = { resp, reader ->
                assert resp.statusLine.statusCode == 200
                println reader
                println reader.text
                render reader as JSON
                return reader as JSON
            }

            response.'404' = {
                render "Not found"
            }
        }
    }
}
